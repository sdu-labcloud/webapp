### build process ###
# base image
FROM node:10-alpine as build

# working directory
WORKDIR /usr/src/app

# copy dependency lists
COPY package.json package-lock.json ./

# install dependencies
RUN npm i

# copy source files
COPY . ./

# build time variables
ARG REACT_APP_API_URL
ARG REACT_APP_HUB_URL
ARG REACT_APP_TAG
ARG REACT_APP_DOCS_URL
ARG REACT_APP_POLL_INTERVAL

# build application
RUN npm run build

### production environment ###
# base image
FROM nginx:mainline-alpine

# remove default virtual host configuration
RUN mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bak

# copy optimized virtual host configuration
COPY config/nginx.conf /etc/nginx/conf.d/default.conf

# copy files from previous stage
COPY --from=build /usr/src/app/build /usr/share/nginx/html

# expose default nginx port
EXPOSE 80

# start nginx server
CMD ["nginx", "-g", "daemon off;"]
