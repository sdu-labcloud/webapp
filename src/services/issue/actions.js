import { snakeCase, get } from 'lodash';
import {
  ISSUE_UPDATE_FIELD,
  ISSUE_VALIDATE_FIELD,
  ISSUE_CREATE_ONE,
  REQUEST,
  ISSUE_CLEAR,
} from '../../constants';
import { ENDPOINT_ISSUE } from '../../endpoints';
import { walkKeys, walkValues, trim } from '../../util';

export const updateField = (field, value) => ({
  type: ISSUE_UPDATE_FIELD,
  field,
  value,
});

export const validateField = (field) => ({
  type: ISSUE_VALIDATE_FIELD,
  field,
});

export const createIssue = () => (dispatch, getState) => {
  const { issue } = getState();
  dispatch({
    type: ISSUE_CREATE_ONE + REQUEST,
    payload: {
      request: {
        method: 'POST',
        url: ENDPOINT_ISSUE(),
        data: walkKeys(snakeCase)(
          walkValues(trim)({
            type: get(issue, 'form.fields.type') || '',
            title: get(issue, 'form.fields.title') || '',
            description: get(issue, 'form.fields.description') || '',
          })
        ),
      },
    },
  });
};

export const clearIssue = () => ({
  type: ISSUE_CLEAR,
});
