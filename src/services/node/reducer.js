import { camelCase } from 'lodash';
import defaultState from './state';
import {
  NODE_READ_MANY,
  NODE_SESSION_UPDATE_ONE,
  REQUEST,
  SUCCESS,
  NODE_DELETE_ONE,
  NODE_UPDATE_ONE,
} from '../../constants';
import { walkKeys, arrayToObject } from '../../util';

const node = (state = defaultState, action = {}) => {
  const { type } = action;
  switch (type) {
    case NODE_READ_MANY + REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case NODE_READ_MANY + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data, count } = action.payload.data;
        const newNodes = arrayToObject(walkKeys(camelCase)(data));
        const nodes = { ...state.nodes, ...newNodes };
        const currentCount = Object.keys(nodes).length;
        const loadedCount = Object.keys(newNodes).length;
        const nextPage =
          loadedCount < state.pageSize ? state.nextPage : state.nextPage + 1;
        return {
          ...state,
          loading: false,
          hasNextPage: count > currentCount,
          nextPage: state.hasNextPage ? nextPage : 0,
          count,
          nodes,
        };
      }
      return state;
    }
    case NODE_SESSION_UPDATE_ONE + REQUEST: {
      const sessions = state.nodes[action.nodeId].sessions.map((session) => {
        if (session.id === action.sessionId) {
          if (action.update.authorized) {
            return {
              ...session,
              authorized: true,
              identify: false,
            };
          }
          return { ...session, ...action.update };
        }
        if (action.update.authorized) {
          return {
            ...session,
            authorized: false,
            identify: false,
          };
        }
        if (action.update.identify) {
          return {
            ...session,
            identify: false,
          };
        }
        return session;
      });
      return {
        ...state,
        nodes: {
          ...state.nodes,
          [action.nodeId]: {
            ...state.nodes[action.nodeId],
            sessions,
          },
        },
      };
    }
    case NODE_SESSION_UPDATE_ONE + REQUEST + SUCCESS: {
      if (action.payload && action.payload.data && action.payload.data.data) {
        const { data } = action.payload.data;
        const sessions = state.nodes[data.node].sessions.map((session) => {
          if (session.id === data.id) {
            return { ...session, ...data };
          }
          return session;
        });
        return {
          ...state,
          nodes: {
            ...state.nodes,
            [data.node]: {
              ...state.nodes[data.node],
              sessions,
            },
          },
        };
      }
      return state;
    }
    case NODE_UPDATE_ONE + REQUEST + SUCCESS: {
      const data = action?.payload?.data?.data;
      if (data) {
        return {
          ...state,
          nodes: {
            ...state.nodes,
            [data.id]: {
              ...state.nodes[data.id],
              releaseChannel: data.release_channel,
            },
          },
        };
      }
      return state;
    }
    case NODE_DELETE_ONE + REQUEST + SUCCESS: {
      const { nodeId } = action.meta.previousAction;
      return {
        ...state,
        count: state.count - 1,
        nodes: Object.keys(state.nodes).reduce((remainingNodes, id) => {
          const nodes = { ...remainingNodes };
          if (id !== nodeId) {
            nodes[id] = state.nodes[id];
          }
          return nodes;
        }, {}),
      };
    }
    default: {
      return state;
    }
  }
};

export default node;
