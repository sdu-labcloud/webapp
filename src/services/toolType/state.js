export default {
  entities: {},
  pageSize: 20,
  nextPage: 0,
  count: 0,
  hasNextPage: true,
  isReadingMany: false,
  isCreatingOne: false,
  isUpdatingOne: false,
  isDeletingOne: false
};
