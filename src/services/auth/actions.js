import { AUTH_LOGIN, AUTH_LOGOUT } from '../../constants';

export const login = token => ({
  type: AUTH_LOGIN,
  token
});

export const logout = () => ({
  type: AUTH_LOGOUT
});
