import { blue, blueGrey } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core';

export default createMuiTheme({
  palette: {
    primary: {
      main: blue['700'],
    },
    secondary: {
      main: blueGrey['100'],
    },
    text: {
      disabled: 'rgba(0, 0, 0, 0.54)',
    },
    status: {
      error: '#f44336',
      warning: '#ff9800',
      success: '#4caf50',
      info: '#2196f3',
    },
  },
  drawer: {
    width: 200,
  },
});
