import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { ENDPOINT_OAUTH2_AZURE_AD_LOGIN } from '../../endpoints';
import { logout } from '../../services/auth/actions';
import { LoadingButton } from '../index';

const styles = (theme) => ({
  button: {
    marginRight: theme.spacing(2),
    backgroundColor: theme.palette.primary.contrastText,
    color: theme.palette.primary.main,
  },
});

class AuthButton extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { dispatchLogout, token } = this.props;
    if (token) {
      dispatchLogout();
    } else {
      this.setState({
        loading: true,
      });
    }
  }

  render() {
    const { loading } = this.state;
    const { token, classes } = this.props;
    const href = token ? '/dashboard' : ENDPOINT_OAUTH2_AZURE_AD_LOGIN();
    /* eslint-disable react/jsx-props-no-spreading */
    const LinkComponent = React.forwardRef((props, ref) =>
      token ? (
        <Link ref={ref} {...props} />
      ) : (
        <a ref={ref} {...props}>
          {props.children}
        </a>
      )
    );
    /* eslint-enable react/jsx-props-no-spreading */
    const text = token ? 'Log out' : 'Log in';
    return token ? (
      <Button
        onClick={this.onClick}
        className={classes.button}
        component={LinkComponent}
        to={href}
        variant="contained"
      >
        {text}
      </Button>
    ) : (
      <LoadingButton
        onClick={this.onClick}
        className={classes.button}
        component={LinkComponent}
        loading={loading}
        disabled={loading}
        href={href}
        progressColor="secondary"
        variant="contained"
      >
        {text}
      </LoadingButton>
    );
  }
}

AuthButton.propTypes = {
  dispatchLogout: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    button: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ auth }) => ({
  token: auth.token,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchLogout: () => dispatch(logout()),
});

export default withStyles(styles)(
  connect(mapStateToProps, mapDispatchToProps)(AuthButton)
);
