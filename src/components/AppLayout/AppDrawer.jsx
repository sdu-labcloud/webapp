import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Drawer,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  withStyles,
} from '@material-ui/core';
import {
  ChevronLeft,
  Dashboard,
  Memory,
  Person,
  Group,
  Print,
  Security,
  Sms,
} from '@material-ui/icons';
import { NavLink } from 'react-router-dom';
import { useIsMobile } from './hooks';

const styles = (theme) => ({
  drawerPaperDefault: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: theme.drawer.width,
    height: '100vh',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: 0,
  },
  drawerToolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  activeListItem: {
    backgroundColor: theme.palette.action.selected,
  },
});

const AppDrawer = ({ classes, open, toggle, user, onClose }) => {
  const isMobile = useIsMobile();
  const drawerVariant = isMobile ? 'temporary' : 'permanent';
  const drawerClasses = { paper: classes.drawerPaperDefault };
  const managementRoles = ['staff', 'admin'];
  /* eslint-disable react/jsx-props-no-spreading */
  const NavLinkComponent = React.forwardRef((props, ref) => (
    <NavLink {...props} innerRef={ref} />
  ));
  /* eslint-enable react/jsx-props-no-spreading */
  return (
    <Drawer
      variant={drawerVariant}
      classes={drawerClasses}
      open={isMobile ? open : true}
      onClose={onClose}
    >
      <div className={classes.drawerToolbar}></div>
      <Divider />
      <ListItem
        activeClassName={classes.activeListItem}
        component={NavLinkComponent}
        to="/dashboard"
        button
      >
        <ListItemIcon>
          <Dashboard />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItem>
      <Divider />
      {user && user.role ? (
        <>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLinkComponent}
            to="/machines"
            button
          >
            <ListItemIcon>
              <Print />
            </ListItemIcon>
            <ListItemText primary="Machines" />
          </ListItem>
          <Divider />
        </>
      ) : null}
      {user && (
        <>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLinkComponent}
            to="/profile"
            button
          >
            <ListItemIcon>
              <Person />
            </ListItemIcon>
            <ListItemText primary="Profile" />
          </ListItem>
          <Divider />
        </>
      )}
      {user && user.role && managementRoles.includes(user.role) ? (
        <>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLinkComponent}
            to="/users"
            button
          >
            <ListItemIcon>
              <Group />
            </ListItemIcon>
            <ListItemText primary="Users" />
          </ListItem>
          <Divider />
        </>
      ) : null}
      {user && user.role && managementRoles.includes(user.role) ? (
        <>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLinkComponent}
            to="/nodes"
            button
          >
            <ListItemIcon>
              <Memory />
            </ListItemIcon>
            <ListItemText primary="Nodes" />
          </ListItem>
          <Divider />
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLinkComponent}
            to="/permissions"
            button
          >
            <ListItemIcon>
              <Security />
            </ListItemIcon>
            <ListItemText primary="Permissions" />
          </ListItem>
          <Divider />
        </>
      ) : null}
      {user ? (
        <>
          <ListItem
            activeClassName={classes.activeListItem}
            component={NavLinkComponent}
            to="/feedback"
            button
          >
            <ListItemIcon>
              <Sms />
            </ListItemIcon>
            <ListItemText primary="Feedback" />
          </ListItem>
          <Divider />
        </>
      ) : null}
    </Drawer>
  );
};

AppDrawer.propTypes = {
  classes: PropTypes.shape({
    drawerPaperDefault: PropTypes.string.isRequired,
    drawerPaperClose: PropTypes.string.isRequired,
    drawerToolbar: PropTypes.string.isRequired,
    activeListItem: PropTypes.string.isRequired,
  }).isRequired,
  open: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    azureId: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  }),
};

AppDrawer.defaultProps = {
  user: null,
};

export default withStyles(styles)(AppDrawer);
