import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import { ENDPOINT_OAUTH2_AZURE_AD_LOGIN } from '../../endpoints';
import { logout } from '../../services/auth/actions';
import LoadingButton from '../LoadingButton';
import styles from './styles';

class AuthButton extends Component {
  constructor() {
    super();
    this.state = {
      loading: false
    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { dispatchLogout, token } = this.props;
    if (token) {
      dispatchLogout();
    } else {
      this.setState({
        loading: true
      });
    }
  }

  render() {
    const { loading } = this.state;
    const { token, color, classes } = this.props;
    /* eslint-disable react/jsx-props-no-spreading */
    const LinkComponent = React.forwardRef((props, ref) =>
      token ? (
        <Link {...props} ref={ref} />
      ) : (
        <a {...props} ref={ref}>
          {props.children}
        </a>
      )
    );
    /* eslint-enable react/jsx-props-no-spreading */
    const href = token ? '/dashboard' : ENDPOINT_OAUTH2_AZURE_AD_LOGIN();
    const text = token ? 'Log out' : 'Log in';
    const buttonColor = color === 'white' ? undefined : color;
    const buttonClassName = clsx(classes.button, {
      [classes.buttonWhite]: color === 'white'
    });
    return token ? (
      <Button
        onClick={this.onClick}
        className={buttonClassName}
        component={LinkComponent}
        to={href}
        color={buttonColor}
        variant='contained'
      >
        {text}
      </Button>
    ) : (
      <LoadingButton
        onClick={this.onClick}
        className={buttonClassName}
        component={LinkComponent}
        loading={loading}
        disabled={loading}
        href={href}
        color={buttonColor}
        progressColor='secondary'
        variant='contained'
      >
        {text}
      </LoadingButton>
    );
  }
}

AuthButton.propTypes = {
  dispatchLogout: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  classes: PropTypes.shape({
    button: PropTypes.string.isRequired,
    buttonWhite: PropTypes.string.isRequired
  }).isRequired,
  color: PropTypes.oneOf(['inherit', 'primary', 'secondary', 'white'])
};

AuthButton.defaultProps = {
  color: 'inherit'
};

const mapStateToProps = ({ auth }) => ({
  token: auth.token
});

const mapDispatchToProps = dispatch => ({
  dispatchLogout: () => dispatch(logout())
});

export default withStyles(styles)(
  connect(mapStateToProps, mapDispatchToProps)(AuthButton)
);
