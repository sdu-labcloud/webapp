const styles = {
  container: {
    flexGrow: 1,
    margin: 0,
    width: '100%',
    height: '100%'
  }
};

export default styles;
