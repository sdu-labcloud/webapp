import React from 'react';
import PropTypes from 'prop-types';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Grid,
  TextField,
  InputAdornment,
  withStyles,
  Button,
  ListItemText,
} from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import { Select } from '../../../../components';
import styles from './styles';

const options = [
  {
    value: 'approved',
    label: 'Approved',
  },
  {
    value: 'denied',
    label: 'Denied',
  },
  {
    value: 'pending',
    label: 'Pending',
  },
];

const Approval = ({ classes, approval, expanded, onClick, history }) => (
  <Accordion expanded={expanded}>
    <AccordionSummary expandIcon={<ExpandMore />} onClick={onClick}>
      <Grid container spacing={2} alignContent="center">
        <Grid xs={12} md={6} item>
          <ListItemText primary="Name" secondary={approval.userName} />
        </Grid>
        <Grid xs={12} md={6} item>
          <ListItemText primary="Machine" secondary={approval.machine} />
        </Grid>
      </Grid>
    </AccordionSummary>
    <AccordionDetails>
      <Grid container spacing={4} className={classes.grid} alignItems="center">
        <Grid xs={12} lg={12} item>
          <TextField
            label="Description"
            variant="outlined"
            inputProps={{ name: 'description' }}
            rows={5}
            value={approval.description}
            multiline
            fullWidth
            disabled
          />
        </Grid>
        <Grid xs={12} lg={6} item>
          <TextField
            id="time with buffer"
            variant="outlined"
            label="Time with buffer"
            defaultValue={approval.hours}
            disabled={history}
            required
            fullWidth
            InputProps={{
              type: 'number',
              endAdornment: (
                <InputAdornment position="start">hours</InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid xs={12} lg={6} item>
          <Select
            options={options}
            label="State"
            variant="outlined"
            value={approval.state}
            disabled
            fullWidth
          />
        </Grid>
        <Grid xs={12} lg={12} item>
          <TextField
            label="Comment"
            variant="outlined"
            inputProps={{ name: 'comment' }}
            rows={5}
            defaultValue={approval.comment}
            disabled={history}
            multiline
            fullWidth
          />
        </Grid>
        {!history ? (
          <>
            <Grid item>
              <Button variant="contained" color="primary">
                Approve
              </Button>
            </Grid>
            <Grid item>
              <Button variant="contained" color="secondary">
                Deny
              </Button>
            </Grid>
          </>
        ) : null}
      </Grid>
    </AccordionDetails>
  </Accordion>
);

Approval.propTypes = {
  approval: PropTypes.shape({
    userName: PropTypes.string.isRequired,
    machine: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    hours: PropTypes.number.isRequired,
    state: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired,
  }).isRequired,
  expanded: PropTypes.bool,
  onClick: PropTypes.func,
  classes: PropTypes.shape({
    grid: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.bool,
};

Approval.defaultProps = {
  history: false,
  expanded: false,
  onClick: () => null,
};

export default withStyles(styles)(Approval);
