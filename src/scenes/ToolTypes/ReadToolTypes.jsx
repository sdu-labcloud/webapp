import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { objectToArray, byProperty } from '../../util';
import { readToolTypes } from '../../services/toolType/actions';
import { FixedLayout } from '../../components';
import { POLL_INTERVAL } from '../../constants';
import ToolTypeBox from './ToolTypeBox';
import CreateToolType from './CreateToolType';

class ReadToolTypes extends Component {
  constructor() {
    super();
    this.timer = null;
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    const { dispatchReadToolTypes, toolTypeIds } = this.props;
    if (!toolTypeIds.length) {
      dispatchReadToolTypes('name');
    }
    if (!this.timer) {
      this.timer = setInterval(() => {
        dispatchReadToolTypes('name');
      }, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  loadMore() {
    const { dispatchReadToolTypes } = this.props;
    dispatchReadToolTypes('name');
  }

  render() {
    const { isReadingMany, hasNextPage, toolTypeIds } = this.props;
    return (
      <FixedLayout>
        <Grid
          component={InfiniteScroll}
          container
          spacing={2}
          alignItems="stretch"
          direction="row"
          loadMore={this.loadMore}
          hasMore={!isReadingMany && hasNextPage}
          useWindow={false}
        >
          {toolTypeIds.map(toolTypeId => (
            <Grid sm={12} md={6} lg={4} item key={toolTypeId}>
              <ToolTypeBox id={toolTypeId} />
            </Grid>
          ))}
        </Grid>
        <CreateToolType />
      </FixedLayout>
    );
  }
}

ReadToolTypes.propTypes = {
  isReadingMany: PropTypes.bool.isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  dispatchReadToolTypes: PropTypes.func.isRequired,
  toolTypeIds: PropTypes.arrayOf(PropTypes.string).isRequired
};

const mapStateToProps = ({ toolType }) => ({
  toolTypeIds: objectToArray(toolType.entities)
    .sort(byProperty('name'))
    .map(item => item.id),
  isReadingMany: toolType.isReadingMany,
  hasNextPage: toolType.hasNextPage
});

const mapDispatchToProps = dispatch => ({
  dispatchReadToolTypes: sortOrder => dispatch(readToolTypes(sortOrder))
});

export default connect(mapStateToProps, mapDispatchToProps)(ReadToolTypes);
