import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withStyles
} from '@material-ui/core';
import { DeleteOutlined } from '@material-ui/icons';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { deleteToolType } from '../../services/toolType/actions';
import { AuthZ } from '../../hocs';
import { deleteImage } from '../../imageRequests';

const styles = theme => ({
  button: {
    margin: `0px ${theme.spacing(0.5)}px`
  }
});

class DeleteToolType extends Component {
  constructor() {
    super();
    this.state = {
      isDialogOpen: false
    };
    this.getEntityNameMarkup = this.getEntityNameMarkup.bind(this);
    this.deleteEntity = this.deleteEntity.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.openDialog = this.openDialog.bind(this);
  }

  getEntityNameMarkup() {
    const { entity } = this.props;
    if (!entity.name) {
      return '';
    }
    return <b>{entity.name}</b>;
  }

  async deleteEntity() {
    const { dispatchDeleteEntity, entity, bearerToken } = this.props;
    if (entity.image) {
      try {
        await deleteImage(bearerToken, entity.image);
      } catch (err) {
        // ignore if image does not exists
      }
    }
    dispatchDeleteEntity(entity.id);
    this.closeDialog();
  }

  openDialog() {
    this.setState(state => ({ ...state, isDialogOpen: true }));
  }

  closeDialog() {
    this.setState(state => ({ ...state, isDialogOpen: false }));
  }

  render() {
    const { entity, classes } = this.props;
    const { isDialogOpen } = this.state;
    return (
      <>
        <Button className={classes.button} onClick={this.openDialog}>
          <DeleteOutlined />
        </Button>
        <Dialog
          open={isDialogOpen}
          onClose={this.closeDialog}
          aria-labelledby={`DeleteToolType_Title_${entity.id}`}
          aria-describedby={`DeleteToolType_Description_${entity.id}`}
        >
          <DialogTitle id={`DeleteToolType_Title_${entity.id}`}>
            Delete this tool type?
          </DialogTitle>
          <DialogContent>
            <DialogContentText
              id={`DeleteToolType_Description_${entity.id}`}
              align="justify"
            >
              The {this.getEntityNameMarkup()} tool type will be permanently
              deleted. Deleting a tool type cannot be undone and will remove all
              existing references. Remaining tools might require configuration
              in order to be accessed afterwards.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} autoFocus>
              Cancel
            </Button>
            <Button onClick={this.deleteEntity} color="primary">
              Delete tool type
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

DeleteToolType.propTypes = {
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  classes: PropTypes.shape({
    button: PropTypes.string.isRequired
  }).isRequired,
  bearerToken: PropTypes.string.isRequired,
  dispatchDeleteEntity: PropTypes.func.isRequired
};

const mapStateToProps = ({ toolType, auth }, { id }) => ({
  bearerToken: auth.token,
  entity: toolType.entities[id]
});

const mapDispatchToProps = dispatch => ({
  dispatchDeleteEntity: id => dispatch(deleteToolType(id))
});

export default compose(
  AuthZ({
    roles: ['staff', 'admin']
  }),
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(DeleteToolType);
