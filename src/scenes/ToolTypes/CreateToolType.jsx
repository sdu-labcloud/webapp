import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Fab,
  Grid,
  LinearProgress,
  TextField,
  withStyles
} from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { withFormik } from 'formik';
import { isFunction } from 'lodash';
import { object, string } from 'yup';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { AuthZ } from '../../hocs';
import { ImageUpload } from '../../components';
import { createToolType } from '../../services/toolType/actions';
import { createImage } from '../../imageRequests';

const styles = theme => ({
  dialogTitleLoading: {
    paddingTop: theme.spacing(3) - 4
  },
  dialogContent: {
    paddingTop: theme.spacing(2)
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing(4),
    // note that the 16 pixels compensate for the scrollbar
    right: 16 + theme.spacing(4)
  }
});

class CreateToolType extends Component {
  constructor(props) {
    super(props);
    this.defaultState = {
      isDialogOpen: false,
      imageUploadState: {
        isDialogOpen: false,
        isBusyCropping: false,
        zoom: 1,
        crop: { x: 0, y: 0 },
        croppedArea: { x: 0, y: 0, width: 0, height: 0 },
        croppedAreaPixels: { x: 0, y: 0, width: 0, height: 0 },
        blob: null,
        filename: '',
        croppedDataUrl: '',
        originalDataUrl: ''
      }
    };
    this.state = this.defaultState;
    this.openDialog = this.openDialog.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.setImageUploadState = this.setImageUploadState.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { isCreatingOne } = this.props;
    if (prevProps.isCreatingOne && !isCreatingOne) {
      this.resetForm();
    }
  }

  setImageUploadState(nextState, callback) {
    this.setState(
      state => ({
        ...state,
        imageUploadState: {
          ...state.imageUploadState,
          ...(isFunction(nextState)
            ? nextState(state.imageUploadState)
            : nextState)
        }
      }),
      callback
    );
  }

  resetForm() {
    const { imageUploadState } = this.defaultState;
    const { resetForm } = this.props;
    resetForm();
    this.setState({ imageUploadState });
  }

  async submitForm() {
    const {
      bearerToken,
      submitForm,
      setFieldValue,
      setSubmitting
    } = this.props;
    const { imageUploadState } = this.state;
    const { blob, filename } = imageUploadState;
    if (blob) {
      setSubmitting(true);
      const res = await createImage(bearerToken, blob, filename);
      if (res.data && res.data.data && res.data.data.id) {
        setFieldValue('image', res.data.data.id, false);
        submitForm();
        this.closeDialog();
      } else {
        setSubmitting(false);
      }
    }
  }

  openDialog() {
    this.setState({ isDialogOpen: true });
  }

  closeDialog() {
    this.setState({ isDialogOpen: false });
  }

  render() {
    const {
      handleBlur,
      handleChange,
      errors,
      values,
      touched,
      isValid,
      isSubmitting,
      isCreatingOne,
      classes
    } = this.props;
    const { isDialogOpen, imageUploadState } = this.state;
    const isLoading = isSubmitting || isCreatingOne;
    return (
      <>
        <Fab
          color='primary'
          aria-label='Create new tool type'
          className={classes.fab}
          onClick={this.openDialog}
        >
          <Add />
        </Fab>
        <Dialog
          open={isDialogOpen}
          onClose={this.closeDialog}
          scroll='body'
          aria-labelledby='CreateToolType_Title'
        >
          {isLoading && <LinearProgress color='primary' />}
          <DialogTitle
            id='CreateToolType_Title'
            className={clsx({
              [classes.dialogTitleLoading]: isLoading
            })}
          >
            Create tool type
          </DialogTitle>
          <DialogContent className={classes.dialogContent}>
            <Grid spacing={4} container>
              <Grid xs={12} item>
                <TextField
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.name}
                  helperText={touched.name && errors.name}
                  error={!!(touched.name && errors.name)}
                  name='name'
                  label='Name'
                  variant='outlined'
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} item>
                <TextField
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.description}
                  helperText={touched.description && errors.description}
                  error={!!(touched.description && errors.description)}
                  name='description'
                  label='Description'
                  variant='outlined'
                  multiline
                  fullWidth
                  rows={5}
                />
              </Grid>
              <Grid xs={12} item>
                <ImageUpload
                  aspectRatio={2}
                  ButtonProps={{ variant: 'contained', color: 'primary' }}
                  value={imageUploadState}
                  onChange={this.setImageUploadState}
                  required
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} autoFocus>
              Cancel
            </Button>
            <Button
              onClick={this.submitForm}
              disabled={!isValid || isLoading || !imageUploadState.blob}
              color='primary'
            >
              Create tool type
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

CreateToolType.propTypes = {
  values: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
  }).isRequired,
  errors: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string
  }).isRequired,
  touched: PropTypes.shape({
    name: PropTypes.bool,
    description: PropTypes.bool
  }).isRequired,
  bearerToken: PropTypes.string.isRequired,
  isCreatingOne: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  setSubmitting: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    dialogTitleLoading: PropTypes.string.isRequired,
    dialogContent: PropTypes.string.isRequired,
    fab: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ toolType, auth }) => ({
  bearerToken: auth.token,
  isCreatingOne: toolType.isCreatingOne
});

const mapDispatchToProps = dispatch => ({
  dispatchCreateEntity: entity => dispatch(createToolType(entity))
});

export default compose(
  AuthZ({
    roles: ['staff', 'admin']
  }),
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
  withFormik({
    displayName: 'CreateToolType',
    handleSubmit: (values, { props, setSubmitting }) => {
      const { dispatchCreateEntity } = props;
      dispatchCreateEntity(values);
      setSubmitting(false);
    },
    mapPropsToValues: () => ({
      name: '',
      description: '',
      image: ''
    }),
    validationSchema: object().shape({
      name: string()
        .strict()
        .trim('The name must not start or end with spaces.')
        .required('The name is required.')
        .min(2, 'The name must be at least two characters long.')
        .max(250, 'The name must not be longer than 250 characters.'),
      description: string()
        .strict()
        .trim('The abbreviation must not start or end with spaces.')
        .max(250, 'The description must not be longer than 250 characters.'),
      image: string().strict()
    })
  })
)(CreateToolType);
