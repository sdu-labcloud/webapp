import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  LinearProgress,
  TextField,
  withStyles
} from '@material-ui/core';
import { EditOutlined } from '@material-ui/icons';
import { withFormik } from 'formik';
import { isFunction, get } from 'lodash';
import { object, string } from 'yup';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { ENDPOINT_IMAGE_CONTENT } from '../../endpoints';
import { AuthZ } from '../../hocs';
import { ImageUpload } from '../../components';
import { updateToolType } from '../../services/toolType/actions';
import { createImage, deleteImage } from '../../imageRequests';

const styles = theme => ({
  dialogTitleLoading: {
    paddingTop: theme.spacing(3) - 4
  },
  dialogContent: {
    paddingTop: theme.spacing(2)
  },
  button: {
    margin: `0px ${theme.spacing(0.5)}px`
  }
});

class UpdateToolType extends Component {
  constructor(props) {
    super(props);
    this.defaultState = {
      isDialogOpen: false,
      imageUploadState: {
        isDialogOpen: false,
        isBusyCropping: false,
        zoom: 1,
        crop: { x: 0, y: 0 },
        croppedArea: { x: 0, y: 0, width: 0, height: 0 },
        croppedAreaPixels: { x: 0, y: 0, width: 0, height: 0 },
        blob: null,
        filename: '',
        croppedDataUrl: '',
        originalDataUrl: ''
      }
    };
    const { entity } = this.props;
    this.state = {
      ...this.defaultState,
      imageUploadState: {
        ...this.defaultState.imageUploadState,
        croppedDataUrl: ENDPOINT_IMAGE_CONTENT(entity.image)
      }
    };
    this.closeDialog = this.closeDialog.bind(this);
    this.openDialog = this.openDialog.bind(this);
    this.applyEntity = this.applyEntity.bind(this);
    this.commitEntity = this.commitEntity.bind(this);
    this.setImageUploadState = this.setImageUploadState.bind(this);
    this.isCroppedImageUpdated = this.isCroppedImageUpdated.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { isUpdatingOne } = this.props;
    if (prevProps.isUpdatingOne && !isUpdatingOne) {
      this.resetForm();
    }
    this.ensureImagePreview(prevProps);
  }

  setImageUploadState(nextState, callback) {
    this.setState(
      state => ({
        ...state,
        imageUploadState: {
          ...state.imageUploadState,
          ...(isFunction(nextState)
            ? nextState(state.imageUploadState)
            : nextState)
        }
      }),
      callback
    );
  }

  resetForm() {
    const { imageUploadState } = this.defaultState;
    const { resetForm } = this.props;
    resetForm();
    this.setState({ imageUploadState });
  }

  ensureImagePreview(prevProps) {
    const { entity } = this.props;
    const { imageUploadState } = this.state;
    const hasImageChanged =
      prevProps.entity.image && prevProps.entity.image !== entity.image;
    const imagePreviewNeedsUpdate =
      entity.image && (!imageUploadState.croppedDataUrl || hasImageChanged);
    if (imagePreviewNeedsUpdate) {
      this.setState((state, props) => ({
        imageUploadState: {
          ...state.imageUploadState,
          croppedDataUrl: ENDPOINT_IMAGE_CONTENT(props.entity.image)
        }
      }));
    }
  }

  isCroppedImageUpdated() {
    const { imageUploadState } = this.state;
    return /^data:/.test(imageUploadState.croppedDataUrl);
  }

  async applyEntity() {
    const {
      bearerToken,
      submitForm,
      setFieldValue,
      setSubmitting,
      entity
    } = this.props;
    const { imageUploadState } = this.state;
    const { blob, filename } = imageUploadState;
    let id = entity.image;
    setSubmitting(true);
    if (this.isCroppedImageUpdated()) {
      try {
        const res = await createImage(bearerToken, blob, filename);
        id = get(res, 'data.data.id', '');
        if (id) {
          try {
            await deleteImage(bearerToken, entity.image);
          } catch (err) {
            // ignore if image does not exists
          }
        }
      } catch (err) {
        // ignore if image cannot be created and keep existing image
      }
    }
    setFieldValue('image', id, false);
    submitForm();
  }

  async commitEntity() {
    await this.applyEntity();
    this.closeDialog();
  }

  openDialog() {
    this.setState({ isDialogOpen: true });
  }

  closeDialog() {
    this.setState({ isDialogOpen: false });
  }

  render() {
    const {
      handleBlur,
      handleChange,
      errors,
      values,
      touched,
      isValid,
      isSubmitting,
      isUpdatingOne,
      classes,
      entity
    } = this.props;
    const { isDialogOpen, imageUploadState } = this.state;
    const isCroppedImageUpdated = this.isCroppedImageUpdated();
    return (
      <>
        <Button className={classes.button} onClick={this.openDialog}>
          <EditOutlined />
        </Button>
        <Dialog
          open={isDialogOpen}
          onClose={this.closeDialog}
          scroll='body'
          aria-labelledby={`UpdateToolType_Title_${entity.id}`}
        >
          {(isSubmitting || isUpdatingOne) && (
            <LinearProgress color='primary' />
          )}
          <DialogTitle
            id={`UpdateToolType_Title_${entity.id}`}
            className={clsx({
              [classes.dialogTitleLoading]: isSubmitting || isUpdatingOne
            })}
          >
            Edit tool type
          </DialogTitle>
          <DialogContent className={classes.dialogContent}>
            <Grid spacing={4} container>
              <Grid xs={12} item>
                <TextField
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.name}
                  helperText={touched.name && errors.name}
                  error={!!(touched.name && errors.name)}
                  name='name'
                  label='Name'
                  variant='outlined'
                  required
                  fullWidth
                />
              </Grid>
              <Grid xs={12} item>
                <TextField
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.description}
                  helperText={touched.description && errors.description}
                  error={!!(touched.description && errors.description)}
                  name='description'
                  label='Description'
                  variant='outlined'
                  multiline
                  fullWidth
                  rows={5}
                />
              </Grid>
              <Grid xs={12} item>
                <ImageUpload
                  aspectRatio={2}
                  ButtonProps={{ variant: 'contained', color: 'primary' }}
                  value={imageUploadState}
                  onChange={this.setImageUploadState}
                  required
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} autoFocus>
              Cancel
            </Button>
            <Button
              onClick={this.applyEntity}
              disabled={!isValid && !isCroppedImageUpdated}
              color='primary'
            >
              Apply
            </Button>
            <Button
              onClick={this.commitEntity}
              disabled={!isValid && !isCroppedImageUpdated}
              color='primary'
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

UpdateToolType.propTypes = {
  values: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
  }).isRequired,
  errors: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string
  }).isRequired,
  touched: PropTypes.shape({
    name: PropTypes.bool,
    description: PropTypes.bool
  }).isRequired,
  bearerToken: PropTypes.string.isRequired,
  isUpdatingOne: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  setSubmitting: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  classes: PropTypes.shape({
    dialogTitleLoading: PropTypes.string.isRequired,
    dialogContent: PropTypes.string.isRequired,
    button: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ toolType, auth }, { id }) => ({
  bearerToken: auth.token,
  isUpdatingOne: toolType.isUpdatingOne,
  entity: toolType.entities[id]
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdateEntity: (id, entity) => dispatch(updateToolType(id, entity))
});

export default compose(
  AuthZ({
    roles: ['staff', 'admin']
  }),
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
  withFormik({
    displayName: 'UpdateToolType',
    handleSubmit: (values, { props, setSubmitting }) => {
      const { dispatchUpdateEntity, id } = props;
      dispatchUpdateEntity(id, values);
      setSubmitting(false);
    },
    mapPropsToValues: ({ entity }) => ({
      name: entity.name,
      description: entity.description,
      image: entity.image
    }),
    validationSchema: object().shape({
      name: string()
        .strict()
        .trim('The name must not start or end with spaces.')
        .required('The name is required.')
        .min(2, 'The name must be at least two characters long.')
        .max(250, 'The name must not be longer than 250 characters.'),
      description: string()
        .strict()
        .trim('The abbreviation must not start or end with spaces.')
        .max(250, 'The description must not be longer than 250 characters.'),
      image: string().strict()
    })
  })
)(UpdateToolType);
