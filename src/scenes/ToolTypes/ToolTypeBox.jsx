import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  withStyles
} from '@material-ui/core';
import { connect } from 'react-redux';
import { ENDPOINT_IMAGE_CONTENT } from '../../endpoints';
import UpdateToolType from './UpdateToolType';
import DeleteToolType from './DeleteToolType';

const styles = {
  card: {
    position: 'relative',
    minHeight: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  image: {
    objectFit: 'cover'
  },
  description: {
    minHeight: 21
  }
};

const ToolTypeBox = ({ classes, toolType }) => (
  <Card className={classes.card}>
    <CardActionArea component={Link} to={`/tool_types/${toolType.id}`}>
      <CardMedia
        component="img"
        className={classes.image}
        image={ENDPOINT_IMAGE_CONTENT(toolType.image)}
        title={toolType.name}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {toolType.name}
        </Typography>
        <Typography className={classes.description} component="p" noWrap>
          {toolType.description}
        </Typography>
      </CardContent>
    </CardActionArea>
    <CardActions>
      <Button
        component={Link}
        to={`/tool_types/${toolType.id}`}
        variant="contained"
        color="primary"
      >
        Show tools
      </Button>
      <UpdateToolType id={toolType.id} />
      <DeleteToolType id={toolType.id} />
    </CardActions>
  </Card>
);

ToolTypeBox.propTypes = {
  classes: PropTypes.shape({
    card: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  toolType: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ toolType }, { id }) => ({
  toolType: toolType.entities[id]
});

export default withStyles(styles)(connect(mapStateToProps)(ToolTypeBox));
