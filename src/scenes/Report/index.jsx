import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  TextField
} from '@material-ui/core';
import { connect } from 'react-redux';
import { FixedLayout, Select } from '../../components';
import { AuthN } from '../../hocs';

const options = [
  {
    value: 'a',
    label: 'SLA-1'
  },
  {
    value: 'b',
    label: 'SLA-2'
  }
];

const Report = ({ profile }) => (
  <FixedLayout>
    <Card>
      <CardHeader
        title="Incident report"
        subheader="Report machine failure or request maintenance"
      />
      <CardContent>
        <Grid spacing={4} container>
          <Grid sm={12} lg={6} item>
            <TextField
              value={profile ? profile.displayName : 'unknown'}
              label="Display name"
              variant="outlined"
              disabled
              fullWidth
            />
          </Grid>
          <Grid sm={12} lg={6} item>
            <TextField
              value={profile ? profile.email : 'unknown'}
              label="Email"
              variant="outlined"
              disabled
              fullWidth
            />
          </Grid>
          <Grid xs={12} item>
            <Select
              options={options}
              value={options[options.length - 1].value}
              label="Machine name"
              name="machine"
              variant="outlined"
              required
              fullWidth
            />
          </Grid>
          <Grid xs={12} item>
            <TextField
              rows={5}
              label="Description"
              placeholder="Briefly describe what happened"
              variant="outlined"
              fullWidth
              required
              multiline
            />
          </Grid>
          <Grid xs={12} item>
            <TextField
              rows={5}
              label="Other information"
              placeholder="Briefly describe if and how you tried to fix it"
              variant="outlined"
              fullWidth
              multiline
            />
          </Grid>
          <Grid xs={12} item>
            <Grid item>
              <Button variant="contained" color="primary" disabled>
                Submit
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  </FixedLayout>
);

Report.propTypes = {
  profile: PropTypes.shape({
    email: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired
  })
};

Report.defaultProps = {
  profile: null
};

const mapStateToProps = ({ user }) => ({ profile: user.profile });

export default AuthN({
  redirect: true,
  redirectTarget: '/dashboard'
})(connect(mapStateToProps)(Report));
