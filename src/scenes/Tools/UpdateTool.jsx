import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  LinearProgress,
  TextField,
  withStyles
} from '@material-ui/core';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { compose } from 'recompose';
import { withFormik } from 'formik';
import { object, string } from 'yup';
import { updateTool } from '../../services/tool/actions';
import ToolTypeSelect from './ToolTypeSelect';

const styles = theme => ({
  dialogTitleLoading: {
    paddingTop: theme.spacing(3) - 4
  },
  dialogContent: {
    paddingTop: theme.spacing(2)
  }
});

const UpdateTool = ({
  open,
  onClose,
  entity,
  isUpdatingOne,
  isSubmitting,
  isValid,
  classes,
  handleBlur,
  handleChange,
  handleSubmit,
  setFieldValue,
  values,
  touched,
  errors
}) => {
  const setTagId = ({ target }) =>
    setFieldValue(target.name, target.value.toUpperCase());
  const submitForm = () => {
    handleSubmit();
    onClose();
  };
  return (
    <Dialog
      open={open}
      onClose={onClose}
      scroll='body'
      aria-labelledby={`UpdateTool_Title_${entity.id}`}
    >
      {(isSubmitting || isUpdatingOne) && <LinearProgress color='primary' />}
      <DialogTitle
        id={`UpdateTool_Title_${entity.id}`}
        className={clsx({
          [classes.dialogTitleLoading]: isSubmitting || isUpdatingOne
        })}
      >
        Edit tool
      </DialogTitle>
      <DialogContent className={classes.dialogContent}>
        <Grid spacing={4} container>
          <Grid xs={12} md={6} item>
            <TextField
              onBlur={handleBlur}
              onChange={setTagId}
              value={values.tagId}
              helperText={touched.tagId && errors.tagId}
              error={!!(touched.tagId && errors.tagId)}
              name='tagId'
              label='RFID tag ID'
              variant='outlined'
              required
              fullWidth
            />
          </Grid>
          <Grid xs={12} md={6} item>
            <ToolTypeSelect
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.type}
              helperText={touched.type && errors.type}
              error={!!(touched.type && errors.type)}
              name='type'
              label='Tool type'
              variant='outlined'
              required
              fullWidth
            />
          </Grid>
          <Grid xs={12} md={6} item>
            <TextField
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.shape}
              helperText={touched.shape && errors.shape}
              error={!!(touched.shape && errors.shape)}
              name='shape'
              label='Tool shape'
              variant='outlined'
              fullWidth
            />
          </Grid>
          <Grid xs={12} md={6} item>
            <TextField
              onBlur={handleBlur}
              onChange={handleChange}
              value={values.size}
              helperText={touched.size && errors.size}
              error={!!(touched.size && errors.size)}
              name='size'
              label='Tool size'
              variant='outlined'
              fullWidth
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} autoFocus>
          Cancel
        </Button>
        <Button onClick={submitForm} disabled={!isValid} color='primary'>
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
};

UpdateTool.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  isUpdatingOne: PropTypes.bool.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  values: PropTypes.shape({
    tagId: PropTypes.string.isRequired,
    size: PropTypes.string.isRequired,
    shape: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
  }).isRequired,
  errors: PropTypes.shape({
    tagId: PropTypes.string,
    size: PropTypes.string,
    shape: PropTypes.string,
    type: PropTypes.string
  }).isRequired,
  touched: PropTypes.shape({
    tagId: PropTypes.bool,
    size: PropTypes.bool,
    shape: PropTypes.bool,
    type: PropTypes.bool
  }).isRequired,
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired,
  classes: PropTypes.shape({
    dialogContent: PropTypes.string.isRequired,
    dialogTitleLoading: PropTypes.string.isRequired
  }).isRequired
};

UpdateTool.defaultProps = {
  open: false,
  onClose: () => null
};

const mapStateToProps = ({ tool }, { id }) => ({
  entity: tool.entities[id],
  isUpdatingOne: tool.isUpdatingOne
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdateEntity: (id, entity) => dispatch(updateTool(id, entity))
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps),
  withFormik({
    displayName: 'UpdateTool',
    handleSubmit: (values, { props, setSubmitting }) => {
      const { dispatchUpdateEntity, id } = props;
      const payload = {
        ...values,
        type: values.type || null
      };
      dispatchUpdateEntity(id, payload);
      setSubmitting(false);
    },
    mapPropsToValues: ({ entity }) => ({
      tagId: entity.tagId,
      size: entity.size,
      shape: entity.shape,
      type: (entity.type && entity.type.id) || ''
    }),
    validationSchema: object().shape({
      tagId: string()
        .strict()
        .uppercase()
        .trim('The tag ID must not start or end with spaces.')
        .required('The tag ID is required.')
        .matches(/^[A-F0-9]{8}$/, {
          excludeEmptyString: true,
          message:
            'The tag ID has to be an 8 character long hexadecimal string.'
        }),
      size: string()
        .strict()
        .trim('The size must not start or end with spaces.')
        .max(50, 'The size must not be longer than 50 characters.'),
      shape: string()
        .strict()
        .trim('The shape must not start or end with spaces.')
        .max(50, 'The shape must not be longer than 50 characters.'),
      type: string()
        .strict()
        .trim('The shape must not start or end with spaces.')
    })
  })
)(UpdateTool);
