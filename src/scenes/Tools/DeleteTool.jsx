import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton
} from '@material-ui/core';
import { DeleteOutline } from '@material-ui/icons';
import { connect } from 'react-redux';
import { deleteTool } from '../../services/tool/actions';

const DeleteTool = ({ entity, dispatchDeleteEntity }) => {
  const [open, setOpen] = useState(false);
  const openDialog = () => setOpen(true);
  const closeDialog = () => setOpen(false);
  const deleteEntity = () => {
    dispatchDeleteEntity(entity.id);
    closeDialog();
  };
  return (
    <>
      <IconButton onClick={openDialog}>
        <DeleteOutline />
      </IconButton>
      <Dialog
        open={open}
        onClose={closeDialog}
        aria-labelledby={`DeleteTool_Title_${entity.id}`}
        aria-describedby={`DeleteTool_Description_${entity.id}`}
      >
        <DialogTitle id={`DeleteTool_Title_${entity.id}`}>
          Delete this tool?
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id={`DeleteTool_Description_${entity.id}`}
            align="justify"
          >
            The tool with the ID <b>{entity.id}</b> and the RFID tag ID{' '}
            <b>{entity.tagId}</b> will be permanently deleted. Deleting a tool
            type cannot be undone. To add the tool to the system again, it needs
            to be reregistered.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog} autoFocus>
            Cancel
          </Button>
          <Button onClick={deleteEntity} color="primary">
            Delete tool
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

DeleteTool.propTypes = {
  dispatchDeleteEntity: PropTypes.func.isRequired,
  entity: PropTypes.shape({
    id: PropTypes.string.isRequired,
    tagId: PropTypes.string.isRequired
  }).isRequired
};

const mapStateToProps = ({ tool }, { id }) => ({
  entity: tool.entities[id]
});

const mapDispatchToProps = dispatch => ({
  dispatchDeleteEntity: id => dispatch(deleteTool(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteTool);
