import { useState } from 'react';
import axios from 'axios';
import { get } from 'lodash';
import { ENDPOINT_TOOL_TYPE } from '../../endpoints';
import { useInitialAndPeriodicAction } from '../../hooks';

function useToolTypeOrNavigate(id, history) {
  const [state, setState] = useState({ loading: false, toolType: null });

  async function getToolTypeOrNavigate() {
    if (!state.loading && !state.toolType) {
      setState(prevState => ({
        ...prevState,
        loading: true
      }));
      try {
        const response = await axios.get(ENDPOINT_TOOL_TYPE(id));
        setState(prevState => ({
          ...prevState,
          loading: false,
          toolType: get(response, 'data.data', null)
        }));
      } catch (err) {
        // Navigate the user to the tool types page, if the tool does not exist.
        history.push('/tool_types');
      }
    }
  }

  // Fetch information about the tool type when mounting.
  useInitialAndPeriodicAction(() => {
    getToolTypeOrNavigate();
  }, 5000);

  return [state, setState];
}

export default useToolTypeOrNavigate;
