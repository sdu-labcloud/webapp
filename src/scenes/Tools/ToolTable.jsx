import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
} from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroller';
import { Check, Close, EditOutlined } from '@material-ui/icons';
import { connect } from 'react-redux';
import { readTools } from '../../services/tool/actions';
import { byProperty, objectToArray } from '../../util';
import { useInitialAndPeriodicAction } from '../../hooks';
import DeleteTool from './DeleteTool';
import UpdateTool from './UpdateTool';

const ToolTable = ({
  tools,
  dispatchReadEntities,
  loading,
  hasNextPage,
  profile,
}) => {
  useInitialAndPeriodicAction(() => {
    dispatchReadEntities('created');
  }, 1000);
  const [open, setOpen] = useState(false);
  const openDialog = () => setOpen(true);
  const closeDialog = () => setOpen(false);
  const loadMore = () => dispatchReadEntities('created');
  const isManager = profile && ['staff', 'admin'].includes(profile.role);
  const displayBorrowedBy = (borrowedBy) => {
    if (!borrowedBy) return <i>Nobody</i>;
    if (typeof borrowedBy === 'string') return borrowedBy;
    if (borrowedBy.displayName) return borrowedBy.displayName;
    return borrowedBy.id || <i>Unknown</i>;
  };
  return (
    <Paper>
      <Table padding="default">
        <TableHead>
          <TableRow>
            <TableCell>Database ID</TableCell>
            <TableCell>RFID tag ID</TableCell>
            <TableCell>Shape</TableCell>
            <TableCell>Size</TableCell>
            <TableCell>Borrowed by</TableCell>
            <TableCell>Borrowed since</TableCell>
            <TableCell>Available</TableCell>
            {isManager && <TableCell>Actions</TableCell>}
          </TableRow>
        </TableHead>
        {tools.length ? (
          <TableBody
            component={InfiniteScroll}
            element="tbody"
            loadMore={loadMore}
            hasMore={!loading && hasNextPage}
            useWindow={false}
          >
            {tools.map((tool) => (
              <TableRow key={tool.id}>
                <TableCell>{tool.id}</TableCell>
                <TableCell>{tool.tagId}</TableCell>
                <TableCell>{tool.shape || <i>None</i>}</TableCell>
                <TableCell>{tool.size || <i>None</i>}</TableCell>
                <TableCell>{displayBorrowedBy(tool.borrowedBy)}</TableCell>
                <TableCell>
                  {(tool.borrowedBy && tool.borrowedSince) || <i>Never</i>}
                </TableCell>
                <TableCell>{tool.available ? <Check /> : <Close />}</TableCell>
                {isManager && (
                  <TableCell>
                    <IconButton onClick={openDialog}>
                      <EditOutlined />
                    </IconButton>
                    <UpdateTool
                      open={open}
                      onClose={closeDialog}
                      id={tool.id}
                    />
                    <DeleteTool id={tool.id} />
                  </TableCell>
                )}
              </TableRow>
            ))}
          </TableBody>
        ) : (
          <TableBody>
            <TableRow>
              <TableCell colSpan={isManager ? 8 : 7}>
                <b>There are currently no tools with this tool type.</b>
              </TableCell>
            </TableRow>
          </TableBody>
        )}
      </Table>
    </Paper>
  );
};

ToolTable.propTypes = {
  dispatchReadEntities: PropTypes.func.isRequired,
  tools: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      tagId: PropTypes.string.isRequired,
      shape: PropTypes.string.isRequired,
      size: PropTypes.string.isRequired,
      borrowedBy: PropTypes.shape({}),
      borrowedSince: PropTypes.string.isRequired,
      available: PropTypes.bool.isRequired,
    }).isRequired
  ).isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  profile: PropTypes.shape({
    role: PropTypes.string.isRequired,
  }),
};

ToolTable.defaultProps = {
  profile: null,
};

const mapStateToProps = ({ tool, user }, { toolTypeId }) => ({
  tools: objectToArray(tool.entities)
    .filter((item) => item.type && item.type.id === toolTypeId)
    .sort(byProperty('name')),
  hasNextPage: tool.hasNextPage,
  loading: tool.isReadingMany,
  profile: user.profile,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchReadEntities: (sortOrder) => dispatch(readTools(sortOrder)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ToolTable);
