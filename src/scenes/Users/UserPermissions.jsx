import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CheckboxSelection } from '../../components';
import { updateUser } from '../../services/user/actions';

const UserPermissions = ({ user, userId, permissions, dispatchUpdateUser }) => {
  const getPermissionDisplayName = id =>
    `${permissions[id].abbreviation}: ${permissions[id].name}`;
  const updateUserPermission = event => {
    const permissionId = event.target.value;
    // Check if permission should be removed or added.
    if (user.permissions.includes(permissionId)) {
      dispatchUpdateUser(userId, {
        permissions: user.permissions.filter(id => id !== permissionId)
      });
    } else {
      dispatchUpdateUser(userId, {
        permissions: [...user.permissions, permissionId]
      });
    }
  };
  return (
    <CheckboxSelection
      key={userId}
      selected={user.permissions}
      options={Object.keys(permissions)}
      transformLabels={getPermissionDisplayName}
      onChange={updateUserPermission}
    />
  );
};

UserPermissions.propTypes = {
  userId: PropTypes.string.isRequired,
  user: PropTypes.shape({
    permissions: PropTypes.arrayOf(PropTypes.string).isRequired
  }).isRequired,
  permissions: PropTypes.shape({}).isRequired,
  dispatchUpdateUser: PropTypes.func.isRequired
};

const mapStateToProps = ({ permission, user }, { userId }) => ({
  user: user.users[userId],
  permissions: permission.permissions
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdateUser: (userId, update) => dispatch(updateUser(userId, update))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPermissions);
