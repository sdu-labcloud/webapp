import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Grid,
  LinearProgress,
  TextField,
  Typography
} from '@material-ui/core';
import { Info } from '@material-ui/icons';
import { connect } from 'react-redux';
import { FixedLayout, Select } from '../../components';
import { DOCS_URL } from '../../endpoints';
import {
  updateField,
  validateField,
  createIssue,
  clearIssue
} from '../../services/issue/actions';
import Issue from './components/Issue';

class Help extends Component {
  constructor() {
    super();
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.getError = this.getError.bind(this);
    this.hasError = this.hasError.bind(this);
  }

  onChange(evt) {
    const { value, name } = evt.target;
    const { dispatchUpdateField } = this.props;
    dispatchUpdateField(name, value);
  }

  onBlur(evt) {
    const { name } = evt.target;
    const { dispatchValidateField } = this.props;
    dispatchValidateField(name);
  }

  getError(field) {
    const { errors } = this.props;
    const error = errors.find(err => err.field === field);
    return (error && error.error) || error;
  }

  hasError(field) {
    return typeof this.getError(field) !== 'undefined';
  }

  render() {
    const {
      profile,
      fields,
      loading,
      valid,
      issue,
      dispatchCreateIssue,
      dispatchClearIssue,
      history
    } = this.props;
    const options = [
      {
        value: 'kind/feature',
        label: 'Feature request'
      },
      {
        value: 'kind/bug',
        label: 'Bug report'
      }
    ];
    const avatar = (
      <Avatar>
        <Info />
      </Avatar>
    );
    return (
      <FixedLayout>
        <Card>
          {loading && <LinearProgress color="secondary" />}
          {history &&
          history.location &&
          history.location.pathname === '/help' ? (
            <>
              <CardHeader title="Documentation" />
              <CardContent>
                <Grid spacing={2} container>
                  <Grid xs={12} item>
                    <Typography align="justify" gutterBottom>
                      The project contains three levels of documentation. User
                      documentation covers the use cases of the everday user,
                      where as administrator documentation helps you with the
                      management of the platform. On top of that developer
                      documentation is the place to start when you want to
                      improve or build upon the platform.
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Button
                      href={DOCS_URL}
                      variant="contained"
                      color="secondary"
                      component="a"
                    >
                      Show documentation
                    </Button>
                  </Grid>
                </Grid>
              </CardContent>
            </>
          ) : null}
          {!issue && (
            <CardHeader
              title="Feature request or bug report"
              subheader="Tell us how we can improve the software"
            />
          )}
          {!issue && profile && (
            <CardContent>
              <Grid spacing={2} container>
                <Grid sm={12} lg={6} item>
                  <TextField
                    inputProps={{ name: 'email' }}
                    value={profile.email}
                    label="Email"
                    variant="outlined"
                    disabled
                    fullWidth
                  />
                </Grid>
                <Grid sm={12} lg={6} item>
                  <TextField
                    inputProps={{ name: 'displayName' }}
                    value={profile.displayName}
                    label="Display name"
                    variant="outlined"
                    disabled
                    fullWidth
                  />
                </Grid>
                <Grid xs={12} item>
                  <Select
                    error={this.hasError('type')}
                    helperText={this.getError('type')}
                    onChange={this.onChange}
                    value={fields.type}
                    options={options}
                    label="Type"
                    name="type"
                    variant="outlined"
                    required
                    fullWidth
                  />
                </Grid>
                <Grid xs={12} item>
                  <TextField
                    error={this.hasError('title')}
                    helperText={this.getError('title')}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                    inputProps={{ name: 'title' }}
                    value={fields.title}
                    label="Title"
                    variant="outlined"
                    required
                    fullWidth
                  />
                </Grid>
                <Grid xs={12} item>
                  <TextField
                    error={this.hasError('description')}
                    helperText={this.getError('description')}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                    inputProps={{ name: 'description' }}
                    value={fields.description}
                    rows={5}
                    label="Description"
                    variant="outlined"
                    required
                    multiline
                    fullWidth
                  />
                </Grid>
                <Grid item>
                  <Button
                    onClick={dispatchCreateIssue}
                    disabled={loading || !valid}
                    variant="contained"
                    color="primary"
                  >
                    Submit feedback
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          )}
          {!profile && (
            <CardContent>
              <Chip
                avatar={avatar}
                label="Please log in to submit a feature request or bug report."
                color="primary"
              />
            </CardContent>
          )}
          {issue && <Issue issue={issue} onClear={dispatchClearIssue} />}
        </Card>
      </FixedLayout>
    );
  }
}

Help.propTypes = {
  loading: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
  dispatchUpdateField: PropTypes.func.isRequired,
  dispatchValidateField: PropTypes.func.isRequired,
  dispatchCreateIssue: PropTypes.func.isRequired,
  dispatchClearIssue: PropTypes.func.isRequired,
  profile: PropTypes.shape({
    email: PropTypes.string.isRequired,
    displayName: PropTypes.string.isRequired
  }),
  issue: PropTypes.shape({
    title: PropTypes.string.isRequired,
    labels: PropTypes.arrayOf(PropTypes.string).isRequired,
    description: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired
  }),
  fields: PropTypes.shape({
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
  }).isRequired,
  errors: PropTypes.arrayOf(
    PropTypes.shape({
      field: PropTypes.string.isRequired,
      error: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  history: PropTypes.shape({
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

Help.defaultProps = {
  profile: null,
  issue: null
};

const mapStateToProps = ({ user, issue }) => ({
  fields: issue.form.fields,
  errors: issue.form.errors,
  loading: issue.form.loading,
  valid:
    !issue.form.errors.length &&
    issue.form.fields.type !== 'none' &&
    issue.form.fields.title !== '' &&
    issue.form.fields.description !== '',
  issue: issue.issue,
  profile: user.profile
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdateField: (field, value) => dispatch(updateField(field, value)),
  dispatchValidateField: field => dispatch(validateField(field)),
  dispatchCreateIssue: () => dispatch(createIssue()),
  dispatchClearIssue: () => dispatch(clearIssue())
});

export default connect(mapStateToProps, mapDispatchToProps)(Help);
