import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  CardContent,
  CardHeader,
  Chip,
  Grid,
  Typography
} from '@material-ui/core';

const Issue = ({ issue, onClear }) => {
  if (issue) {
    return (
      <>
        <CardHeader title={issue.title} />
        <CardContent>
          <Grid spacing={2} container>
            <Grid item>
              <Grid spacing={8} container>
                {issue.labels.map(label => (
                  <Grid key={label} item>
                    <Chip color="default" label={label} />
                  </Grid>
                ))}
              </Grid>
            </Grid>
            <Grid xs={12} item>
              <Typography align="justify" gutterBottom>
                {issue.description.split('##### Description\n').pop()}
              </Typography>
            </Grid>
            <Grid item>
              <Button onClick={onClear} variant="contained" color="secondary">
                Create another issue
              </Button>
            </Grid>
            <Grid item>
              <Button
                component="a"
                href={issue.link}
                variant="contained"
                color="primary"
              >
                Show issue in GitLab
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </>
    );
  }
  return null;
};

Issue.propTypes = {
  issue: PropTypes.shape({
    title: PropTypes.string.isRequired,
    labels: PropTypes.arrayOf(PropTypes.string).isRequired,
    description: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired
  }),
  onClear: PropTypes.func
};

Issue.defaultProps = {
  issue: null,
  onClear: () => null
};

export default Issue;
