import React from 'react';
import PropTypes from 'prop-types';
import {
  Divider,
  Accordion,
  AccordionActions,
  AccordionDetails,
  AccordionSummary,
  Grid,
  Typography,
  withStyles,
} from '@material-ui/core';
import { Check, Close, ExpandMore, NotInterested } from '@material-ui/icons';
import { connect } from 'react-redux';
import { ENDPOINT_IMAGE_CONTENT } from '../../endpoints';
import UpdatePermission from './UpdatePermission';
import DeletePermission from './DeletePermission';
import { schedulingOptions, workflowOptions, byValue } from './common';

const styles = (theme) => ({
  thumbnail: {
    width: theme.spacing(8),
  },
});

const ExpansionCardPermission = ({
  expanded,
  permission,
  onClick,
  classes,
}) => (
  <Accordion expanded={expanded}>
    <AccordionSummary expandIcon={<ExpandMore />} onClick={onClick}>
      <Grid container spacing={3} alignItems="center">
        <Grid item>
          {permission.image ? (
            <img
              className={classes.thumbnail}
              src={ENDPOINT_IMAGE_CONTENT(permission.image)}
              alt={`${permission.name}`}
            />
          ) : (
            <NotInterested className={classes.thumbnail} fontSize="large" />
          )}
        </Grid>
        <Grid item>
          <Typography variant="subtitle2">{permission.name}</Typography>
          <Typography>{permission.abbreviation}</Typography>
        </Grid>
      </Grid>
    </AccordionSummary>
    <Divider />
    <AccordionDetails>
      <Grid container spacing={8} alignItems="flex-start">
        <Grid xs={6} sm={4} md={3} item>
          <Typography variant="subtitle2">Name</Typography>
        </Grid>
        <Grid xs={6} sm={8} md={9} item>
          <Typography>{permission.name}</Typography>
        </Grid>
        <Grid xs={6} sm={4} md={3} item>
          <Typography variant="subtitle2">Abbreviation</Typography>
        </Grid>
        <Grid xs={6} sm={8} md={9} item>
          <Typography>{permission.abbreviation}</Typography>
        </Grid>
        <Grid xs={6} sm={4} md={3} item>
          <Typography variant="subtitle2">Description</Typography>
        </Grid>
        <Grid xs={6} sm={8} md={9} item>
          <Typography>{permission.description || <i>None</i>}</Typography>
        </Grid>
        <Grid xs={6} sm={4} md={3} item>
          <Typography variant="subtitle2">Scheduling</Typography>
        </Grid>
        <Grid xs={6} sm={8} md={9} item>
          <Typography>
            {schedulingOptions.find(byValue(permission.scheduling)).label}
          </Typography>
        </Grid>
        <Grid xs={6} sm={4} md={3} item>
          <Typography variant="subtitle2">Workflow</Typography>
        </Grid>
        <Grid xs={6} sm={8} md={9} item>
          <Typography>
            {workflowOptions.find(byValue(permission.workflow)).label}
          </Typography>
        </Grid>
        <Grid xs={6} sm={4} md={3} item>
          <Typography variant="subtitle2">Default permission</Typography>
        </Grid>
        <Grid xs={6} sm={8} md={9} item>
          <Typography>{permission.default ? <Check /> : <Close />}</Typography>
        </Grid>
      </Grid>
    </AccordionDetails>
    <Divider />
    <AccordionActions>
      <UpdatePermission id={permission.id} />
      <DeletePermission id={permission.id} />
    </AccordionActions>
  </Accordion>
);

ExpansionCardPermission.propTypes = {
  expanded: PropTypes.bool,
  onClick: PropTypes.func,
  permission: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    scheduling: PropTypes.string.isRequired,
    workflow: PropTypes.string.isRequired,
    default: PropTypes.bool.isRequired,
  }).isRequired,
  classes: PropTypes.shape({
    thumbnail: PropTypes.string.isRequired,
  }).isRequired,
};

ExpansionCardPermission.defaultProps = {
  expanded: false,
  onClick: () => null,
};

const mapStateToProps = ({ permission }, { id }) => ({
  permission: permission.permissions[id],
});

export default withStyles(styles)(
  connect(mapStateToProps)(ExpansionCardPermission)
);
