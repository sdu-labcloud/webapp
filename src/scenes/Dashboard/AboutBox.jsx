import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Grid,
  Typography,
  withStyles
} from '@material-ui/core';
import { AuthButton } from '../../components';

const styles = {
  card: {
    minHeight: '100%'
  }
};

const AboutBox = ({ classes }) => (
  <Card className={classes.card}>
    <CardHeader title="About this page" />
    <CardContent>
      <Grid spacing={2} container>
        <Grid xs={12} item>
          <Typography align="justify" gutterBottom>
            This is the official webpage of the student workshop at SDU
            Sønderborg. It offers a booking system for all manufacturing devices
            available. To be able to use this system, you need to log in with
            your SDU user account.
          </Typography>
        </Grid>
        <Grid item>
          <AuthButton color="primary" />
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);

AboutBox.propTypes = {
  classes: PropTypes.shape({
    card: PropTypes.string.isRequired
  }).isRequired
};

export default withStyles(styles)(AboutBox);
