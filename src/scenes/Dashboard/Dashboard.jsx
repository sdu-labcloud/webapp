import React from 'react';
import { Grid } from '@material-ui/core';
import { FixedLayout } from '../../components';
import AboutBox from './AboutBox';

const Dashboard = () => {
  return (
    <FixedLayout>
      <Grid spacing={2} container>
        <Grid xs={12} item>
          <AboutBox />
        </Grid>
      </Grid>
    </FixedLayout>
  );
};

export default Dashboard;
