import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Grid,
  withStyles,
} from '@material-ui/core';
import InfiniteScroll from 'react-infinite-scroller';
import clsx from 'clsx';
import { FixedLayout } from '../../components';
import { readNodes } from '../../services/node/actions';
import { byProperty, objectToArray } from '../../util';
import { POLL_INTERVAL } from '../../constants';
import NodeDetail from './NodeDetail';
import { AuthZ } from '../../hocs';
import styles from './styles';

class Nodes extends Component {
  constructor() {
    super();
    this.state = {
      expanded: '',
    };
    this.timer = null;
    this.toggleExpansion = this.toggleExpansion.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    const { dispatchReadNodes, nodeIds } = this.props;
    if (!nodeIds.length) {
      dispatchReadNodes('created');
    }
    if (!this.timer) {
      this.timer = setInterval(() => {
        dispatchReadNodes('created');
      }, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  toggleExpansion(panel) {
    return () => {
      this.setState((state) => ({
        expanded: state.expanded === panel ? '' : panel,
      }));
    };
  }

  loadMore() {
    const { dispatchReadNodes } = this.props;
    dispatchReadNodes('created');
  }

  render() {
    const { nodeIds, loading, hasNextPage, classes } = this.props;
    const { expanded } = this.state;
    return (
      <FixedLayout>
        <Card>
          <CardHeader
            title="Nodes"
            subheader="Connected nodes for machine control"
          />
          <CardContent>
            {nodeIds.length ? (
              <InfiniteScroll
                loadMore={this.loadMore}
                hasMore={!loading && hasNextPage}
                useWindow={false}
              >
                {nodeIds.map((nodeId) => (
                  <NodeDetail
                    key={nodeId}
                    id={nodeId}
                    expanded={expanded === nodeId}
                    onClick={this.toggleExpansion}
                  />
                ))}
              </InfiniteScroll>
            ) : null}
            {loading && !nodeIds.length ? (
              <Grid
                className={clsx({ [classes.loader]: nodeIds.length > 0 })}
                spacing={2}
                justify="center"
                container
              >
                <Grid item>
                  <CircularProgress color="secondary" />
                </Grid>
              </Grid>
            ) : null}
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

Nodes.propTypes = {
  nodeIds: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  loading: PropTypes.bool.isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  dispatchReadNodes: PropTypes.func.isRequired,
  classes: PropTypes.shape({
    loader: PropTypes.string.isRequired,
  }).isRequired,
};

const mapStateToProps = ({ node }) => ({
  hasNextPage: node.hasNextPage,
  loading: node.loading,
  nodeIds: objectToArray(node.nodes)
    .sort(byProperty('created'))
    .map((n) => n.id),
});

const mapDispatchToProps = (dispatch) => ({
  dispatchReadNodes: (sortBy) => dispatch(readNodes(sortBy)),
});

export default AuthZ({
  roles: ['staff', 'admin'],
  redirect: true,
  redirectTarget: '/dashboard',
})(withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Nodes)));
