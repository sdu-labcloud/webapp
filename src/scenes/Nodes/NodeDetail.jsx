import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Grid,
  ListItemText,
  Typography,
  withStyles,
  Button,
  AccordionActions,
} from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import { startCase } from 'lodash';
import { connect } from 'react-redux';
import { TimeAgo, Switch, Select } from '../../components';
import {
  updateNodeSession,
  deleteNode,
  updateNode,
} from '../../services/node/actions';
import { readMachine } from '../../services/machine/actions';
import NodeUpdateStatus from './NodeUpdateStatus';
import NewSessionStatus from './NewSessionStatus';
import { readPermission } from '../../services/permission/actions';

const styles = (theme) => ({
  listItemText: {
    marginLeft: theme.spacing(2),
  },
  grid: {
    paddingRight: theme.spacing(4),
  },
});

const NodeDetail = ({
  id,
  node,
  expanded,
  onClick,
  dispatchUpdateNodeSession,
  dispatchDeleteNode,
  dispatchReadMachine,
  dispatchReadPermission,
  dispatchUpdateNode,
  machine,
  permission,
  classes,
}) => {
  const toggle = (sessionId) => ({ target }) => {
    if (node.id && sessionId) {
      dispatchUpdateNodeSession(node.id, sessionId, {
        [target.name]: target.checked,
      });
    }
  };
  const macAddressText =
    node.macAddress && node.macAddress.match(/.{2}/g).join(':');
  const onDelete = async (e) => {
    e.stopPropagation();
    dispatchDeleteNode(node.id);
  };

  const machineId = node ? node.machine : '';
  const machineLoaded = machine !== null;
  const permissionId = machine ? machine.permission : '';
  const permissionLoaded = permission !== null;

  useEffect(() => {
    if (machineId && !machineLoaded) {
      dispatchReadMachine(machineId);
    }
  }, [machineId, dispatchReadMachine, machineLoaded]);

  useEffect(() => {
    if (permissionId && !permissionLoaded) {
      dispatchReadPermission(permissionId);
    }
  }, [permissionId, dispatchReadPermission, permissionLoaded]);

  const [releaseChannels, setReleaseChannels] = useState([
    { value: node.releaseChannel, label: node.releaseChannel },
  ]);

  const onUpdateReleaseChannel = (e) => {
    dispatchUpdateNode(node.id, { releaseChannel: e.target.value });
  };

  useEffect(() => {
    let mounted = true;

    (async () => {
      try {
        if (node.applicationName) {
          const projectId = encodeURIComponent(
            `sdu-labcloud/${node.applicationName}`
          );
          const res = await axios.get(
            `https://gitlab.com/api/v4/projects/${projectId}/repository/branches`
          );

          if (mounted) {
            setReleaseChannels(
              res.data.map((branch) => ({
                value: branch.name,
                label: branch.name,
              }))
            );
          }
        }
      } catch (err) {
        // Ignore error, the default value will be used.
      }
    })();

    return () => {
      mounted = false;
    };
  }, [node.applicationName]);

  const mostRecentSessionDate = node.sessions.reduce((mostRecent, session) => {
    if (!mostRecent || session.created > mostRecent) {
      return session.created;
    }
    return mostRecent;
  }, '');

  return (
    <Accordion expanded={expanded} onChange={onClick(id)}>
      <AccordionSummary expandIcon={<ExpandMore />}>
        <Grid container alignItems="center" spacing={2}>
          <Grid xs={12} sm={6} md item>
            <ListItemText
              primary="Machine"
              secondary={
                machine && permission
                  ? `${permission.abbreviation}-${machine.number}`
                  : node.machine || 'None'
              }
            />
          </Grid>
          <Grid xs={12} sm={6} md item>
            <ListItemText
              primary="Created"
              secondary={<TimeAgo date={node.created} />}
            />
          </Grid>
          <Grid xs={12} sm={6} md item>
            <ListItemText
              primary="Most recent session"
              secondary={
                mostRecentSessionDate ? (
                  <TimeAgo date={mostRecentSessionDate} />
                ) : (
                  'No sessions'
                )
              }
            />
          </Grid>
          <Grid xs={12} sm={6} md item>
            <NodeUpdateStatus node={node}></NodeUpdateStatus>
          </Grid>
          <Grid xs={12} sm={6} md item>
            <NewSessionStatus sessions={node.sessions}></NewSessionStatus>
          </Grid>
        </Grid>
      </AccordionSummary>
      <AccordionDetails>
        <Grid spacing={2} className={classes.grid} container>
          <Grid sm={12} md={3} item>
            <ListItemText
              primary="Serial number"
              secondary={node.serialNumber}
            />
          </Grid>
          <Grid xs={12} md={3} item>
            <ListItemText primary="MAC address" secondary={macAddressText} />
          </Grid>
          <Grid xs={12} md={3} item>
            <ListItemText
              primary="Operating system"
              secondary={startCase(node.operatingSystem)}
            />
          </Grid>
          <Grid xs={12} md={3} item>
            <Select
              value={node.releaseChannel}
              options={releaseChannels}
              onChange={onUpdateReleaseChannel}
              label="Release channel"
              name="Release channel"
              variant="outlined"
              required
              fullWidth
            />
          </Grid>
          <Grid sm={12} md={6} item>
            <Grid alignItems="center" container>
              <Grid xs={12} item>
                <Typography variant="body1">Sessions</Typography>
              </Grid>
              {node.sessions.length ? (
                <>
                  <Grid xs={4} item>
                    <Typography variant="body2" color="textSecondary">
                      Authorized
                    </Typography>
                  </Grid>
                  <Grid xs={4} item>
                    <Typography variant="body2" color="textSecondary">
                      Created
                    </Typography>
                  </Grid>
                  <Grid xs={4} item>
                    <Typography variant="body2" color="textSecondary">
                      Blink LED
                    </Typography>
                  </Grid>
                  {node.sessions.map((session) => (
                    <Fragment key={session.id}>
                      <Grid xs={4} item>
                        <Switch
                          onClick={toggle(session.id)}
                          name="authorized"
                          checked={session.authorized}
                          color="primary"
                        />
                      </Grid>
                      <Grid xs={4} item>
                        <Typography>
                          <TimeAgo date={session.created} />
                        </Typography>
                      </Grid>
                      <Grid xs={4} item>
                        <Switch
                          onClick={toggle(session.id)}
                          name="identify"
                          checked={session.identify}
                          color="primary"
                        />
                      </Grid>
                    </Fragment>
                  ))}
                </>
              ) : (
                <Grid xs={12} item>
                  <Typography color="textSecondary">
                    There are no session available.
                  </Typography>
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>
      </AccordionDetails>
      <AccordionActions>
        <Button color="primary" variant="outlined" onClick={onDelete}>
          Delete
        </Button>
      </AccordionActions>
    </Accordion>
  );
};

NodeDetail.propTypes = {
  id: PropTypes.string.isRequired,
  node: PropTypes.shape({
    id: PropTypes.string.isRequired,
    machine: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    created: PropTypes.string.isRequired,
    serialNumber: PropTypes.string.isRequired,
    macAddress: PropTypes.string.isRequired,
    operatingSystem: PropTypes.string.isRequired,
    sessions: PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.object, PropTypes.string])
    ),
  }).isRequired,
  dispatchUpdateNodeSession: PropTypes.func.isRequired,
  expanded: PropTypes.bool,
  onClick: PropTypes.func,
  classes: PropTypes.shape({
    listItemText: PropTypes.string.isRequired,
    grid: PropTypes.string.isRequired,
  }).isRequired,
};

NodeDetail.defaultProps = {
  expanded: false,
  onClick: () => null,
};

const mapStateToProps = ({ node, machine, permission }, { id }) => {
  const thisNode = node.nodes[id];
  const thisMachine = (thisNode && machine.machines[thisNode.machine]) || null;
  const thisPermission =
    (thisMachine && permission.permissions[thisMachine.permission]) || null;
  return {
    node: thisNode,
    machine: thisMachine,
    permission: thisPermission,
  };
};

const mapDispatchToProps = (dispatch) => ({
  dispatchUpdateNodeSession: (nodeId, sessionId, update) =>
    dispatch(updateNodeSession(nodeId, sessionId, update)),
  dispatchDeleteNode: (nodeId) => dispatch(deleteNode(nodeId)),
  dispatchReadMachine: (machineId) => dispatch(readMachine(machineId)),
  dispatchReadPermission: (permissionId) =>
    dispatch(readPermission(permissionId)),
  dispatchUpdateNode: (nodeId, update) => dispatch(updateNode(nodeId, update)),
});

export default withStyles(styles)(
  connect(mapStateToProps, mapDispatchToProps)(NodeDetail)
);
