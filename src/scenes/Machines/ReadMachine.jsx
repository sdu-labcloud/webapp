import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Grid,
  withStyles,
  ListItemText,
  AccordionActions,
  Tooltip,
} from '@material-ui/core';
import { ExpandMore, Lens } from '@material-ui/icons';
import { connect } from 'react-redux';
import { capitalize, get } from 'lodash';
import { Select } from '../../components';
import { readNodes } from '../../services/node/actions';
import { readUser } from '../../services/user/actions';
import { deleteMachine, updateMachine } from '../../services/machine/actions';
import { readPermission } from '../../services/permission/actions';

const statusOptions = ['ready', 'maintenance', 'busy'].map((opt) => ({
  value: opt,
  label: capitalize(opt),
}));

const styles = (theme) => ({
  listItemText: {
    marginLeft: theme.spacing(1.5),
  },
  grid: {
    paddingRight: theme.spacing(4),
  },
  ready: {
    fill: theme.palette.success.main,
    fontSize: 32,
  },
  maintenance: {
    color: theme.palette.warning.main,
    fontSize: 32,
  },
  busy: {
    color: theme.palette.error.main,
    fontSize: 32,
  },
  iconGridContainer: {
    height: '100%',
  },
  iconGridItem: {
    height: 32,
  },
});

const ReadMachine = ({
  expanded,
  machine,
  classes,
  onClick,
  permission,
  dispatchReadPermission,
  dispatchDeleteMachine,
  dispatchReadNodes,
  dispatchUpdateMachine,
  dispatchReadUser,
  nodeSerial,
  nodeMac,
  loading,
  user,
  role,
}) => {
  const permissionId = machine ? machine.permission : '';
  const permissionLoaded = permission !== null;

  useEffect(() => {
    if (permissionId && !permissionLoaded) {
      dispatchReadPermission(permissionId);
    }
  }, [permissionId, dispatchReadPermission, permissionLoaded]);

  const onDelete = () => {
    dispatchDeleteMachine(machine.id);
  };

  useEffect(() => {
    if (!loading) {
      dispatchReadNodes('created');
    }
    // eslint-disable-next-line
  }, [dispatchReadNodes]);

  const userLoaded = user !== null;
  useEffect(() => {
    if (machine.user && !userLoaded) {
      dispatchReadUser(machine.user);
    }
  }, [machine.user, dispatchReadUser, userLoaded]);

  const isManager = ['admin', 'staff'].includes(role);

  const onStatusChange = (e) => {
    dispatchUpdateMachine(machine.id, { status: e.target.value });
  };

  const onRelease = () => {
    dispatchUpdateMachine(machine.id, { user: null, status: 'ready' });
  };

  return (
    <Accordion expanded={expanded}>
      <AccordionSummary
        expandIcon={isManager ? <ExpandMore /> : undefined}
        onClick={isManager ? onClick : () => null}
      >
        <Grid container spacing={0}>
          <Grid item xs={2} sm={1} md={1} lg={1} xl={1}>
            <Grid
              className={classes.iconGridContainer}
              justify="center"
              alignItems="center"
              container
            >
              <Grid className={classes.iconGridItem} item>
                <Tooltip title={capitalize(machine.status)}>
                  <Lens className={classes[machine.status]}></Lens>
                </Tooltip>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs sm md lg xl>
            <Grid container spacing={2} alignItems="center">
              <Grid item sm={12} md={4}>
                <ListItemText
                  className={classes.listItemText}
                  primary="Name"
                  secondary={
                    permission
                      ? `${permission.abbreviation}-${machine.number}`
                      : 'unknown'
                  }
                />
              </Grid>
              <Grid item sm={12} md={4}>
                <ListItemText
                  className={classes.listItemText}
                  primary="Current user"
                  secondary={user ? user.displayName : 'Nobody'}
                />
              </Grid>
              <Grid item sm={12} md={4}>
                {isManager ? (
                  <Select
                    value={machine.status}
                    options={statusOptions}
                    onChange={onStatusChange}
                    label="Status"
                    name="Status"
                    variant="outlined"
                    required
                    fullWidth
                  />
                ) : (
                  <ListItemText
                    className={classes.listItemText}
                    primary="Status"
                    secondary={capitalize(machine.status)}
                  />
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </AccordionSummary>
      <AccordionDetails>
        <Grid
          container
          spacing={4}
          className={classes.grid}
          alignItems="center"
        >
          <Grid xs={12} lg={6} item>
            <ListItemText
              className={classes.listItemText}
              primary="Node serial number"
              secondary={nodeSerial}
            ></ListItemText>
          </Grid>
          <Grid xs={12} lg={6} item>
            <ListItemText
              className={classes.listItemText}
              primary="Node MAC address"
              secondary={nodeMac}
            ></ListItemText>
          </Grid>
        </Grid>
      </AccordionDetails>
      {isManager && (
        <AccordionActions>
          <Button variant="outlined" onClick={onDelete}>
            Delete
          </Button>
          <Button color="primary" variant="outlined" onClick={onRelease}>
            Release
          </Button>
        </AccordionActions>
      )}
    </Accordion>
  );
};

ReadMachine.propTypes = {
  expanded: PropTypes.bool,
  onClick: PropTypes.func,
  classes: PropTypes.shape({
    listItemText: PropTypes.string.isRequired,
    grid: PropTypes.string.isRequired,
  }).isRequired,
};

ReadMachine.defaultProps = {
  expanded: false,
  onClick: () => null,
};

const mapStateToProps = ({ machine, permission, user, node }, { id }) => {
  const thisMachine = machine.machines[id];
  const thisNode = node.nodes[thisMachine.node] || null;
  const thisUser = user.users[thisMachine.user] || null;
  return {
    machine: thisMachine,
    user: thisUser,
    nodeSerial: (thisNode && thisNode.serialNumber) || 'Unknown',
    nodeMac:
      (thisNode &&
        thisNode.macAddress &&
        thisNode.macAddress.match(/.{2}/g).join(':')) ||
      'Unknown',
    loading: node.loading,
    permission: permission.permissions[thisMachine.permission] || null,
    role: get(user, 'profile.role', ''),
  };
};

const mapDispatchToProps = (dispatch) => ({
  dispatchReadPermission: (permissionId) =>
    dispatch(readPermission(permissionId)),
  dispatchDeleteMachine: (machineId) => dispatch(deleteMachine(machineId)),
  dispatchReadNodes: (sortBy) => dispatch(readNodes(sortBy)),
  dispatchUpdateMachine: (machineId, update) =>
    dispatch(updateMachine(machineId, update)),
  dispatchReadUser: (userId) => dispatch(readUser(userId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ReadMachine));
