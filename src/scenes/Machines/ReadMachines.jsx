import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Grid,
  withStyles,
  CircularProgress,
} from '@material-ui/core';
import { connect } from 'react-redux';
import clsx from 'clsx';
import InfiniteScroll from 'react-infinite-scroller';
import { byProperty, objectToArray } from '../../util';
import { FixedLayout } from '../../components';
import { POLL_INTERVAL } from '../../constants';
import { readMachines } from '../../services/machine/actions';
import ReadMachine from './ReadMachine';
import CreateMachine from './CreateMachine';

const styles = (theme) => ({
  card: {
    marginTop: theme.spacing(2),
  },
  listItemText: {
    marginLeft: theme.spacing(1.5),
  },
  loader: {
    marginTop: theme.spacing(2),
  },
});

class Machines extends Component {
  constructor() {
    super();
    this.state = {
      expanded: '',
    };
    this.timer = null;
    this.toggleExpansion = this.toggleExpansion.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    const { dispatchReadMachines, machineIds } = this.props;
    if (!machineIds.length) {
      dispatchReadMachines('created');
    }
    if (!this.timer) {
      this.timer = setInterval(() => {
        dispatchReadMachines('created');
      }, POLL_INTERVAL);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  toggleExpansion(machineId) {
    return (e) => {
      e.stopPropagation();
      this.setState((state) => ({
        expanded: state.expanded === machineId ? '' : machineId,
      }));
    };
  }

  loadMore() {
    const { dispatchReadMachines } = this.props;
    dispatchReadMachines('created');
  }

  render() {
    const { classes, machineIds, loading, hasNextPage, profile } = this.props;
    const { expanded } = this.state;
    return (
      <FixedLayout>
        {profile && ['admin', 'staff'].includes(profile.role) ? (
          <CreateMachine></CreateMachine>
        ) : null}
        <Card className={classes.card}>
          <CardHeader title="Machines" subheader="Available machines" />
          <CardContent>
            {machineIds.length ? (
              <InfiniteScroll
                loadMore={this.loadMore}
                hasMore={!loading && hasNextPage}
                useWindow={false}
              >
                {machineIds.map((machineId) => (
                  <ReadMachine
                    key={machineId}
                    id={machineId}
                    expanded={expanded === machineId}
                    onClick={this.toggleExpansion(machineId)}
                  />
                ))}
              </InfiniteScroll>
            ) : null}
            {loading && !machineIds.length ? (
              <Grid
                className={clsx({
                  [classes.loader]: machineIds.length > 0,
                })}
                spacing={2}
                justify="center"
                container
              >
                <Grid item>
                  <CircularProgress color="secondary" />
                </Grid>
              </Grid>
            ) : null}
          </CardContent>
        </Card>
      </FixedLayout>
    );
  }
}

Machines.propTypes = {
  classes: PropTypes.shape({
    card: PropTypes.string.isRequired,
    listItemText: PropTypes.string.isRequired,
    loader: PropTypes.string.isRequired,
  }).isRequired,
  machineIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  hasNextPage: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  dispatchReadMachines: PropTypes.func.isRequired,
};

const mapStateToProps = ({ machine, user }) => ({
  hasNextPage: machine.hasNextPage,
  loading: machine.loading,
  machineIds: objectToArray(machine.machines)
    .sort(byProperty('created'))
    .map((p) => p.id),
  profile: user.profile,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchReadMachines: (sortBy) => dispatch(readMachines(sortBy)),
});

export default withStyles(styles)(
  connect(mapStateToProps, mapDispatchToProps)(Machines)
);
