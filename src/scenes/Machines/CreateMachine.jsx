import React, { useEffect } from 'react';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Fab from '@material-ui/core/Fab';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import Select from '../../components/Select';
import { readPermissions } from '../../services/permission/actions';
import { readNodes } from '../../services/node/actions';
import {
  readMachineNew,
  setMachineNew,
  openMachineForm,
  createMachine,
} from '../../services/machine/actions';
import { objectToArray } from '../../util';

const useStyles = makeStyles((theme) => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing(4),
    // note that the 16 pixels compensate for the scrollbar
    right: 16 + theme.spacing(4),
    zIndex: theme.zIndex.appBar,
  },
}));

const CreateMachine = ({
  permissionOptions,
  nodeOptions,
  newMachine,
  open,
  dispatchReadNodes,
  dispatchReadPermissions,
  dispatchReadMachineNew,
  dispatchSetMachineNew,
  dispatchCreateMachine,
  dispatchOpenMachineForm,
  permissions,
}) => {
  const classes = useStyles();
  const onPermission = (e) => {
    dispatchSetMachineNew({ permission: e.target.value });
  };
  const onNode = (e) => {
    dispatchSetMachineNew({ node: e.target.value });
  };
  const onOpen = () => dispatchOpenMachineForm(true);
  const onClose = () => dispatchOpenMachineForm(false);

  useEffect(() => {
    dispatchReadPermissions('created');
  }, [dispatchReadPermissions]);

  useEffect(() => {
    dispatchReadNodes('created');
  }, [dispatchReadNodes]);

  useEffect(() => {
    if (newMachine.permission) {
      dispatchReadMachineNew(newMachine.permission);
    }
  }, [newMachine.permission, dispatchReadMachineNew]);

  const newMachineName =
    newMachine.permission && newMachine.number > 0
      ? `${permissions[newMachine.permission].abbreviation}-${
          newMachine.number
        }`
      : 'displayed once you have selected a permission';

  return (
    <>
      <Fab
        className={classes.fab}
        onClick={onOpen}
        color="primary"
        aria-label="add"
      >
        <AddIcon />
      </Fab>
      {open && (
        <Dialog
          onClose={onClose}
          maxWidth="md"
          fullWidth
          aria-labelledby="simple-dialog-title"
          open={open}
        >
          <DialogTitle id="simple-dialog-title">
            Create a new machine
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Select a permission for which you want to create a machine.
            </DialogContentText>
            {permissionOptions.length ? (
              <Select
                label="Permission"
                variant="outlined"
                fullWidth
                required
                value={newMachine.permission}
                onChange={onPermission}
                options={permissionOptions}
              ></Select>
            ) : (
              <DialogContentText>Loading ...</DialogContentText>
            )}
          </DialogContent>
          <DialogContent>
            <DialogContentText>
              The name of the new machine will be <b>{newMachineName}</b>.
            </DialogContentText>
          </DialogContent>
          <DialogContent>
            {nodeOptions.length ? (
              <Select
                label="Node"
                variant="outlined"
                fullWidth
                required
                disabled={newMachine.number === 0}
                value={newMachine.node}
                onChange={onNode}
                options={nodeOptions}
              ></Select>
            ) : (
              <DialogContentText>Loading ...</DialogContentText>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={onClose}>Cancel</Button>
            <Button
              color="primary"
              disabled={newMachine.number === 0}
              onClick={dispatchCreateMachine}
            >
              {newMachine.permission && newMachine.number > 0
                ? `Create ${newMachineName}`
                : 'Create'}
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
};

const mapStateToProps = ({ permission, machine, node }) => ({
  permissionOptions: objectToArray(permission.permissions).map((item) => ({
    value: item.id,
    label: `${item.abbreviation} - ${item.name}`,
  })),
  nodeOptions: objectToArray(node.nodes).map((item) => ({
    value: item.id,
    label: item.serialNumber,
  })),
  permissions: permission.permissions,
  newMachine: machine.new,
  open: machine.newFormOpen,
});

const mapDispatchToProps = (dispatch) => ({
  dispatchReadPermissions: (sortBy) => dispatch(readPermissions(sortBy)),
  dispatchReadMachineNew: (permissionId) =>
    dispatch(readMachineNew(permissionId)),
  dispatchReadNodes: (sortBy) => dispatch(readNodes(sortBy)),
  dispatchSetMachineNew: (newMachine) => dispatch(setMachineNew(newMachine)),
  dispatchOpenMachineForm: (open) => dispatch(openMachineForm(open)),
  dispatchCreateMachine: () => dispatch(createMachine()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateMachine);
