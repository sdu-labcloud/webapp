import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import { API_URL } from '../endpoints';
import { logout } from '../services/auth/actions';

// create new axios client
const client = axios.create({
  baseURL: `${API_URL}/v1`,
  responseType: 'json'
});

// configure axios middleware
const config = {
  // returnRejectedPromiseOnError: true,
  interceptors: {
    request: [
      {
        // inject authentification token into request if available
        success: ({ getState }, req) => {
          const authorizedRequest = req;
          const { token } = getState().auth;
          if (token) {
            authorizedRequest.headers.Authorization = `Bearer ${token}`;
          }
          return authorizedRequest;
        }
      }
    ],
    response: [
      {
        error: ({ dispatch }, error) => {
          if (error.response && error.response.status) {
            const { status } = error.response;
            // log user out if token expired
            if (status === 403) {
              dispatch(logout());
            }
          }
          return Promise.reject(error);
        }
      }
    ]
  }
};

export default axiosMiddleware(client, config);
