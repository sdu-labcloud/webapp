import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import {
  auth,
  user,
  issue,
  node,
  permission,
  machine,
  toolType,
  tool
} from './services';
import axiosMiddleware from './middlewares/axios';
import cookieMiddleware, { getInitialState } from './middlewares/cookie';

// combine different reducers to store
const reducers = combineReducers({
  auth,
  user,
  issue,
  node,
  permission,
  machine,
  toolType,
  tool
});

// fetch default state state defined in reducers
export const getDefaultState = createStore(reducers).getState;

// create initial state from default state and cookies
const initialState = getInitialState(getDefaultState());

// combine middlewares to enable usage in store
const middlewares = composeWithDevTools(
  applyMiddleware(cookieMiddleware, axiosMiddleware, thunkMiddleware)
);

export default createStore(reducers, initialState, middlewares);
