import { isObjectLike } from 'lodash';

export const walkKeys = func => obj => {
  if (isObjectLike(obj)) {
    if (Array.isArray(obj)) {
      return obj.map(walkKeys(func));
    }
    return Object.keys(obj).reduce((prev, key) => {
      const next = prev;
      const value = obj[key];
      if (isObjectLike(value)) {
        next[func(key)] = walkKeys(func)(value);
      } else {
        next[func(key)] = value;
      }
      return next;
    }, {});
  }
  return obj;
};

export const walkValues = func => obj => {
  if (isObjectLike(obj)) {
    if (Array.isArray(obj)) {
      return obj.map(walkValues(func));
    }
    return Object.keys(obj).reduce((prev, key) => {
      const next = prev;
      const value = obj[key];
      if (isObjectLike(value)) {
        next[key] = walkValues(func)(value);
      } else {
        next[key] = func(value);
      }
      return next;
    }, {});
  }
  return obj;
};
